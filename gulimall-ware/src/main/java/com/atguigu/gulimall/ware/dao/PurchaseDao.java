package com.atguigu.gulimall.ware.dao;

import com.atguigu.gulimall.ware.entity.PurchaseEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 采购信息
 * 
 * @author SQW
 * @email 123@qq.com
 * @date 2021-04-24 14:50:14
 */
@Mapper
public interface PurchaseDao extends BaseMapper<PurchaseEntity> {
	
}
