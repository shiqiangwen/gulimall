package com.atguigu.gulimall.ware.dao;

import com.atguigu.gulimall.ware.entity.UndoLogEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * 
 * @author SQW
 * @email 123@qq.com
 * @date 2021-04-24 14:50:14
 */
@Mapper
public interface UndoLogDao extends BaseMapper<UndoLogEntity> {
	
}
